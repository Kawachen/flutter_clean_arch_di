import 'package:dartz/dartz.dart';
import 'package:di_test/core/failure.dart';
import 'package:di_test/domain/entity/joke.dart';

abstract class JokeRepository {
  Future<Either<Failure, Joke>> getRandomJoke();
}

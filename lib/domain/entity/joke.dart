import 'package:equatable/equatable.dart';

class Joke extends Equatable {
  final String id;
  final String joke;

  const Joke({required this.id, required this.joke});

  @override
  List<Object?> get props => [id, joke];

  @override
  bool get stringify => true;
}

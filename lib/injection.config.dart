// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart' as _i1;
import 'package:injectable/injectable.dart' as _i2;

import 'data/joke_repository.dart' as _i4;
import 'domain/repository/joke_repository.dart' as _i3;
import 'presentation/joke/joke_cubit.dart'
    as _i5; // ignore_for_file: unnecessary_lambdas

// ignore_for_file: lines_longer_than_80_chars
/// initializes the registration of provided dependencies inside of [GetIt]
_i1.GetIt $initGetIt(_i1.GetIt get,
    {String? environment, _i2.EnvironmentFilter? environmentFilter}) {
  final gh = _i2.GetItHelper(get, environment, environmentFilter);
  gh.singleton<_i3.JokeRepository>(_i4.JokeRepositoryImpl());
  gh.factory<_i5.JokeCubit>(() => _i5.JokeCubit(get<_i3.JokeRepository>()));
  return get;
}

import 'package:equatable/equatable.dart';

abstract class JokeState extends Equatable {
  @override
  List<Object?> get props => [];
}

class Empty extends JokeState {}

class Loading extends JokeState {}

class Error extends JokeState {}

class Loaded extends JokeState {
  final String joke;

  Loaded(this.joke);

  @override
  List<Object?> get props => [joke];
}

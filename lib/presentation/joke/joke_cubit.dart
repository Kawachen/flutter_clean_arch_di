import 'package:di_test/domain/repository/joke_repository.dart';
import 'package:di_test/presentation/joke/joke_state.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:injectable/injectable.dart';

@injectable
class JokeCubit extends Cubit<JokeState> {
  final JokeRepository jokeRepository;

  JokeCubit(this.jokeRepository) : super(Empty());

  Future<void> loadRandomJoke() async {
    emit(Loading());
    final jokeResult = await jokeRepository.getRandomJoke();

    jokeResult.fold(
      (failure) => emit(Error()),
      (joke) => emit(Loaded(joke.joke)),
    );
  }
}

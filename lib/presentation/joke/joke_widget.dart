import 'package:di_test/injection.dart';
import 'package:di_test/presentation/joke/joke_cubit.dart';
import 'package:di_test/presentation/joke/joke_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class JokeWidget extends StatelessWidget {
  final JokeCubit cubit;

  JokeWidget({Key? key, JokeCubit? cubit})
      : cubit = cubit ?? sl.get<JokeCubit>(),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider<JokeCubit>(
      create: (_) => cubit..loadRandomJoke(),
      child: BlocBuilder<JokeCubit, JokeState>(
        builder: (_, state) {
          if (state is Loaded) {
            return Text(state.joke);
          }

          return Container();
        },
      ),
    );
  }
}

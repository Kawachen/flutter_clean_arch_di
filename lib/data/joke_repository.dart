import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:di_test/core/failure.dart';
import 'package:di_test/domain/entity/joke.dart';
import 'package:di_test/domain/repository/joke_repository.dart';
import 'package:http/http.dart';
import 'package:injectable/injectable.dart';

import 'model/joke_dto.dart';

@Singleton(as: JokeRepository)
class JokeRepositoryImpl implements JokeRepository {
  final Uri baseUrl = Uri.parse('https://icanhazdadjoke.com/');
  final Client _client;

  JokeRepositoryImpl(): _client = Client();

  @override
  Future<Either<Failure, Joke>> getRandomJoke() async {
    final response = await _client.get(baseUrl, headers: <String, String>{'Accept': 'application/json'});

    if (response.statusCode != 200) {
      return Left(ServerFailure());
    }

    final jokeModel = JokeDTO.fromJson(json.decode(response.body) as Map<String, dynamic>);
    return Right(Joke(id: jokeModel.id, joke: jokeModel.joke));
  }
}

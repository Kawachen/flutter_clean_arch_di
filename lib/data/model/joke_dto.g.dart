// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'joke_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

JokeDTO _$JokeModelFromJson(Map<String, dynamic> json) => JokeDTO(
      id: json['id'] as String,
      joke: json['joke'] as String,
    );

Map<String, dynamic> _$JokeModelToJson(JokeDTO instance) => <String, dynamic>{
      'id': instance.id,
      'joke': instance.joke,
    };

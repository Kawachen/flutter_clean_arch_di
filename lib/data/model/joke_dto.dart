import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'joke_dto.g.dart';

@JsonSerializable()
class JokeDTO extends Equatable {
  final String id;
  final String joke;

  const JokeDTO({required this.id, required this.joke});

  @override
  List<Object?> get props => [id, joke];

  factory JokeDTO.fromJson(Map<String, dynamic> json) => _$JokeModelFromJson(json);

  Map<String, dynamic> toJson() => _$JokeModelToJson(this);
}

import 'package:di_test/domain/repository/joke_repository.dart';
import 'package:mocktail/mocktail.dart';

class MockJokeRepository extends Mock implements JokeRepository {}

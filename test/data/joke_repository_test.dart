import 'dart:convert';

import 'package:dartz/dartz.dart';
import 'package:di_test/core/failure.dart';
import 'package:di_test/data/joke_repository.dart';
import 'package:di_test/domain/entity/joke.dart';
import 'package:di_test/domain/repository/joke_repository.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:http/http.dart';
import 'package:mocktail/mocktail.dart';

import '../utils.dart';

class MockClient extends Mock implements Client {}

Future<void> main() async {
  final jokeJson = await loadFileAsJson('valid_joke.json');
  const joke = Joke(id: 'some-id', joke: 'some-joke');

  late Client mockClient;
  late JokeRepository jokeRepository;

  group('JokeRepository', () {
    setUpAll(() {
      registerFallbackValue(Uri());
    });

    setUp(() {
      mockClient = MockClient();
      jokeRepository = JokeRepositoryImpl();
    });

    group('getRandomJoke', () {
      test('should return Joke', () async {
        when(() => mockClient.get(any())).thenAnswer((_) => Future.value(Response(json.encode(jokeJson), 200)));

        final result = await jokeRepository.getRandomJoke();
        expect(result, const Right(joke));
      });

      test('should return ServerFailure when response code is not 200', () async {
        when(() => mockClient.get(any())).thenAnswer((_) => Future.value(Response('Some Server Error', 404)));

        final result = await jokeRepository.getRandomJoke();
        expect(result, Left(ServerFailure()));
      });
    });
  });
}

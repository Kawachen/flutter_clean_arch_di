import 'package:di_test/data/model/joke_dto.dart';
import 'package:flutter_test/flutter_test.dart';

import '../../utils.dart';

Future<void> main() async {
  final jokeJson = await loadFileAsJson('valid_joke.json');
  const jokeModel = JokeDTO(id: 'some-id', joke: 'some-joke');

  group('JokeModel', () {
    test('fromJson should create JokeModel from valid json', () {
      final actualJokeModel = JokeDTO.fromJson(jokeJson);
      expect(actualJokeModel, jokeModel);
    });

    test('toJson should return valid json from JokeModel', () {
      final actualJson = jokeModel.toJson();
      expect(actualJson, jokeJson);
    });
  });
}

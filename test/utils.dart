import 'dart:convert';
import 'dart:io';

Future<Map<String, dynamic>> loadFileAsJson(String fileName) async => json.decode(await File('test_resources/$fileName').readAsString());

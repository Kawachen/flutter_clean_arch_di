import 'package:bloc_test/bloc_test.dart';
import 'package:di_test/presentation/joke/joke_cubit.dart';
import 'package:di_test/presentation/joke/joke_state.dart';
import 'package:di_test/presentation/joke/joke_widget.dart';
import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

class MockJokeCubit extends Mock implements JokeCubit {}

void main() {
  const someJoke = 'some-joke';

  group('JokeWidget', () {
    late JokeCubit mockCubit;

    setUp(() {
      mockCubit = MockJokeCubit();

      when(() => mockCubit.loadRandomJoke()).thenAnswer((_) => Future.value());
      when(() => mockCubit.state).thenAnswer((_) => Empty());
      when(() => mockCubit.close()).thenAnswer((_) => Future.value());
    });

    testWidgets('should show joke on Loaded state', (WidgetTester tester) async {
      whenListen(
        mockCubit,
        Stream.fromIterable([Loaded(someJoke)]),
      );

      await tester.pumpWidget(MaterialApp(
        home: JokeWidget(cubit: mockCubit),
      ));
      await tester.pumpAndSettle();

      expect(find.text(someJoke), findsOneWidget);
    });
  });
}

import 'package:bloc_test/bloc_test.dart';
import 'package:dartz/dartz.dart';
import 'package:di_test/core/failure.dart';
import 'package:di_test/domain/entity/joke.dart';
import 'package:di_test/domain/repository/joke_repository.dart';
import 'package:di_test/presentation/joke/joke_cubit.dart';
import 'package:di_test/presentation/joke/joke_state.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:mocktail/mocktail.dart';

import '../../data/joke_repository_mock.dart';

void main() {
  const joke = Joke(id: 'some-id', joke: 'some-joke');

  late JokeRepository jokeRepository;

  group('JokeCubit', () {
    setUp(() {
      jokeRepository = MockJokeRepository();
    });

    JokeCubit _setupCubit() => JokeCubit(jokeRepository);

    group('loadRandomJoke', () {
      blocTest<JokeCubit, JokeState>(
        'should emit Loaded state when Joke is returned',
        build: _setupCubit,
        setUp: () => when(() => jokeRepository.getRandomJoke()).thenAnswer((_) => Future.value(const Right(joke))),
        act: (cubit) => cubit.loadRandomJoke(),
        expect: () => <JokeState>[
          Loading(),
          Loaded(joke.joke),
        ],
      );

      blocTest<JokeCubit, JokeState>(
        'should emit Error state when ServerFailure is returned',
        build: _setupCubit,
        setUp: () => when(() => jokeRepository.getRandomJoke()).thenAnswer((_) => Future.value(Left(ServerFailure()))),
        act: (cubit) => cubit.loadRandomJoke(),
        expect: () => <JokeState>[
          Loading(),
          Error(),
        ],
      );
    });
  });
}
